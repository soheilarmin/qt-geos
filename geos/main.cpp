#include <QtCore>
#include "geos/geom.h"
#include "geos/io/WKTReader.h"
#include "geos/io/WKTWriter.h"
#include "geos/operation/intersection/Rectangle.h"
#include "geos/operation/intersection/RectangleIntersection.h"

int main(int argc, char *argv[])
{

    QCoreApplication app(argc, argv);

    using namespace geos::operation::intersection;
    using namespace geos::geom;


    //Creates a geometry from WKT. For better performance, you will probably need to do it manually but pushing points.
    //
    geos::io::WKTReader wktReader;
    auto polygon1 = wktReader.read("POLYGON ((51.4103507995605 35.7401031893707,51.4128398895264 35.7364107731867,51.4367008209229 35.7420538306804,51.4273452758789 35.7260989685195,51.4446830749512 35.7423324898231,51.4269161224365 35.7520849453137,51.4103507995605 35.7401031893707))");


    //The rectangle to clip the polygon.
    //
    auto rect = Rectangle(51.4185047149659,35.7283286610341,51.4346408843994,35.7502042076139);

    //Applying the clipping. Above values of polygon and rectangle will result in two polygons.
    //
    auto intersected = RectangleIntersection::clip(*polygon1,rect);

    //Printing the results in WKT format
    //
    for(size_t i = 0 ; i < intersected->getNumGeometries() ; ++i){
        geos::io::WKTWriter wktWriter;
        auto part = wktWriter.write(intersected->getGeometryN(i));
        qDebug() << QString::fromStdString(part);
    }

    QTimer::singleShot(0,&app,QCoreApplication::quit);

    return app.exec();
}
